# Webhook to MQTT

A server that receives webhooks and publishes the payload to MQTT.

## Motivation

MQTT is a lightweight publish/subscribe protocol that allows for all sorts of (serverless) applications.  
This server helps you route webhook-payloads to the right MQTT-topics.

## Usage

1. clone the repository
1. configure `filters.edn`
1. deploy to Heroku
1. provision the [CloudMQTT](https://elements.heroku.com/addons/cloudmqtt) add-on
1. send all the webhooks to `https://heroku-url/api/events`

## Filtering

See `filters.edn` for usage.

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run

## License

Copyright © 2017 FIXME
