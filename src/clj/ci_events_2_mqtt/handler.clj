(ns ci-events-2-mqtt.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [ci-events-2-mqtt.routes.services :refer [service-routes]]
            [compojure.route :as route]
            [ci-events-2-mqtt.env :refer [defaults]]
            [mount.core :as mount]
            [ci-events-2-mqtt.middleware :as middleware]))

(mount/defstate init-app
                :start ((or (:init defaults) identity))
                :stop  ((or (:stop defaults) identity)))

(def app-routes
  (routes
    #'service-routes
    (route/not-found
      "page not found")))


(defn app [] (middleware/wrap-base #'app-routes))
