(ns ci-events-2-mqtt.routes.services
  (:require [ring.util.http-response :refer :all]
            [compojure.api.sweet :refer :all]
            [clojure.tools.logging :as log]
            [ci-events-2-mqtt.mqtt :refer [mqtt] :as mqtt]
            [ci-events-2-mqtt.config :refer [env]]
            [ci-events-2-mqtt.struct-filter :as sf]
            [schema.core :as s]))

(s/defschema Map {s/Any s/Any})

(defn config [] (read-string (slurp "filters.edn")))

(defn topic []
  (get-in (config) [:pipeline-events :topic]))

(defn matching-topics [& body]
  (let [configs (vals (config))]
    (map :topic (filter #(seq ((sf/generate-filter (:filters %1)) body)) configs))))


(defapi service-routes
  {:swagger {:ui "/swagger-ui"
             :spec "/swagger.json"
             :data {:info {:version "1.0.0"
                           :title "Sample API"
                           :description "Sample Services"}}}}
  
  (context "/api" []
    :tags ["thingie"]

    (GET "/plus" []
      :return       Long
      :query-params [x :- Long, {y :- Long 1}]
      :summary      "x+y with query-parameters. y defaults to 1."
      (ok (+ x y)))

    (GET "/times/:x/:y" []
      :return      s/Any
      :path-params [x :- Long, y :- Long]
      :summary     "x*y with path-parameters"
      (ok {:env env :url (-> env :cloudmqtt-url) :system (System/getenv)}))

    (POST "/events" []
      :body [body Map]
      :summary     "Events via webhook"
      (ok (map (partial mqtt/publish-body mqtt body) (matching-topics body))))

    (GET "/power" []
      :return      Long
      :header-params [x :- Long, y :- Long]
      :summary     "x^y with header-parameters"
      (ok (long (Math/pow x y))))))
