(ns ci-events-2-mqtt.mqtt
  (:require [ci-events-2-mqtt.config :refer [env]]
            [clojure.string :as str]
            [selmer.parser :as parser]
            [selmer.filters :as sef]
            [clojurewerkz.machine-head.client :as mh]
            [clojure.data.json :as json]
            [mount.core :refer [args defstate]]))

(sef/add-filter! :capture
                 (fn [val re-str]
                  (let [re (re-pattern re-str)] (str (last (flatten (list (re-find re val))))))))

(defn config
  ([]
    (config (-> env :cloudmqtt-url)))
  ([url]
   (let [[_ creds host-port] (->> url (re-find #"\/\/([^@]+)@(.+)$"))
         [user pw] (str/split creds #":")
         host-port (str "tcp://" host-port)]
    {:host-port host-port :credentials {:username user :password pw}})))

(defn connect
  ([] (connect (config)))
  ([config]
    (mh/connect (:host-port config) (mh/generate-id) (:credentials config))))

(defn disconnect [connection]
    (mh/disconnect connection))

(defstate mqtt :start (connect) :stop (disconnect mqtt))

(defn publish-body [conn body topic-template]
  (let [payload (json/write-str body)
        topic (parser/render topic-template body)]
    (do
      (mh/publish conn topic payload 1 true)
      topic)))
