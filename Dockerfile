FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/ci-events-2-mqtt.jar /ci-events-2-mqtt/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/ci-events-2-mqtt/app.jar"]
