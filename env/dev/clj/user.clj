(ns user
  (:require [mount.core :as mount]
            ci-events-2-mqtt.core))

(defn start []
  (mount/start-without #'ci-events-2-mqtt.core/http-server
                       #'ci-events-2-mqtt.core/repl-server))

(defn stop []
  (mount/stop-except #'ci-events-2-mqtt.core/http-server
                     #'ci-events-2-mqtt.core/repl-server))

(defn restart []
  (stop)
  (start))


