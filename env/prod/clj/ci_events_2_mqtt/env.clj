(ns ci-events-2-mqtt.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[ci-events-2-mqtt started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[ci-events-2-mqtt has shut down successfully]=-"))
   :middleware identity})
