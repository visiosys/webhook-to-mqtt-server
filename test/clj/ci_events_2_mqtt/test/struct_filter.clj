(ns ci-events-2-mqtt.test.struct-filter
  (:require [clojure.test :refer :all]
            [ci-events-2-mqtt.struct-filter :as sf]))

(deftest pred
  (letfn [(assert-pred [pred expected] (is ((sf/pred pred) expected)))
          (refute-pred [pred expected] (is (not ((sf/pred pred) expected))))]
  (testing "eq"
    (are [pred expected] (assert-pred pred expected)
         [:eq 3] 3
         [:eq 2] 2
         ))
  (testing "not eq"
    (are [pred expected] (refute-pred pred expected)
         [:eq 3] 1
         [:eq 2] 1
         ))
  (testing "lt"
    (are [pred expected] (assert-pred pred expected)
         [:lt 3] 2
         [:lt 2] 1
         ))
  (testing "not lt"
    (are [pred expected] (refute-pred pred expected)
         [:lt 3] 3
         [:lt 3] 4
         ))
  (testing "matches"
    (are [pred expected] (assert-pred pred expected)
         [:matches #"foo"] "foo is here!"
         [:matches #"world"] "hello world"
         ))
  (testing "not matches"
    (are [pred expected] (refute-pred pred expected)
         [:matches #"world"] "hello foo"
         [:matches #"^moar"] "no moar"
         ))
  (testing "present"
    (are [pred expected] (assert-pred pred expected)
         [:present] "hello"
         [:present] ""
         [:present] 1
         [:present] false
         ))
  (testing "not present"
    (are [pred expected] (refute-pred pred expected)
         [:present] nil
         ))
  (testing "absent"
    (are [pred expected] (assert-pred pred expected)
         [:absent] nil
         ))
  (testing "not absent"
    (are [pred expected] (refute-pred pred expected)
         [:absent] 1
         [:absent] "some"
         [:absent] false
         ))
  (testing "include"
    (are [pred expected] (assert-pred pred expected)
         [:include "val"] ["some" "val"]
         [:include 1] [1]
         ))
  (testing "not include"
    (are [pred expected] (refute-pred pred expected)
         [:include ""] []
         [:include nil] []
         [:include nil] nil
         ))
  (testing "not"
    (are [pred expected] (assert-pred pred expected)
         [:not [:eq ""]] 2
         [:not [:lt 3]] 4
         [:not [:gt 5]] 4
         [:not [:present]] nil
         [:not [:absent]] false
         [:not [:matches #"foo"]] "bar"
         [:not [:include ""]] []
         [:not [:not [:eq 1]]] 1
         ))
  (testing "not not"
    (are [pred expected] (refute-pred pred expected)
         [:not [:eq ""]] ""
         [:not [:matches #"foo"]] "foo"
         ))
))

(deftest attr
  (letfn [(assert-attr [pred expected] (is ((sf/attr pred) expected)))
          (refute-attr [pred expected] (is (not ((sf/attr pred) expected))))]
  (testing "a shallow attribute"
    (are [pred expected] (assert-attr pred expected)
         [:price [:present]] {:price 2}
         [:price [:present]] {:price 1}
         ))
  (testing "a deep attribute"
    (are [pred expected] (assert-attr pred expected)
         [[:price :dollar] [:present]] {:price {:dollar 2}}
         [[:price :dollar] [:present]] {:price {:dollar 1}}
         ))
  (testing "no presence"
    (are [pred expected] (refute-attr pred expected)
         [:price [:present]] {:price nil}
         [:price [:present]] {:no-price 1}
         [:price [:present]] {}
         [[:price :moar] [:present]] {:price {:bar 1}}
         [[:price :moar] [:present]] {:price {}}
         ))
))

(deftest generate-filter
  (letfn [(assert-filter [pred expected] (is (seq ((sf/generate-filter pred) expected))))
          (refute-filter [pred expected] (is (not (seq ((sf/generate-filter pred) expected)))))]
  (testing "yielding results"
    (are [pred expected] (assert-filter pred expected)
         [[:price [:present]] [:price [:lt 2]]]   [{:price 1}]
         [[:price [:present]] [:price [:gt 2]]]   [{:price 3}]
         ))
  (testing "yielding empty results"
    (are [pred expected] (refute-filter pred expected)
         [[:price [:present]] [:price [:lt 2]]]   [{:price 2}]
         [[:price [:present]] [:price [:lt 2]]]   [{}]
         [[:price [:present]] [:bar [:lt 2]]]     [{:price 1}]
         ))
))
